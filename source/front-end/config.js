"use strict";



const frontEndConfig = {
	/**
	 * The application name
	 * @type { string }
	 */
	TITLE: "Marshall Spainhour",

	/**
	 * The index.html <meta> description
	 * @type { string }
	 */
	DESCRIPTION: "A rock garden of silicon, carefully tended.",

	/**
	 * The port used by the Webpack dev server.
	 * @type { number }
	 * @link https://webpack-v3.jsx.app/configuration/dev-server/#devserver-port
	 */
	DEV_PORT: 4443,
};



module.exports = {
	frontEndConfig
};
