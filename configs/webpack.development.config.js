"use strict";

// Webpack Utilities
const { merge } = require("webpack-merge");

// Project Constructs
const paths = require("./paths.config");
const commonWebpackConfiguration = require(paths.webpackCommonConfig);
const { frontEndConfig } = require(paths.frontEndConfig);



module.exports = merge(commonWebpackConfiguration, {

	mode: "development",

	// Documentation: https://webpack.js.org/configuration/devtool
	devtool: "eval-source-map",

	module: {
		rules: [

			// Javascript (Babel)
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
					options: {
						envName: "development",
						configFile: paths.babelConfig,
					},
				},
			},

			// SASS & CSS
			{
				test: /\.(scss|css)$/,
				use: [

					// Inlines all styles into the bundles
					{
						// Documentation: https://www.npmjs.com/package/style-loader
						loader: "style-loader",
						options: {},
					},

					// Interprets CSS import statements
					{
						// Documentation: https://www.npmjs.com/package/css-loader
						loader: "css-loader",
						options: {
							// The number of loaders below this one that should be applied to imported CSS
							importLoaders: 2,
						},
					},

					// Processes CSS through PostCSS
					{
						// Documentation: https://www.npmjs.com/package/postcss-loader
						loader: "postcss-loader",
						options: {
							postcssOptions: {
								config: paths.postcssConfig,
							},
						},
					},

					// Interprets SCSS and CSS files, and compiles SCSS
					{
						// Documentation: https://www.npmjs.com/package/sass-loader
						loader: "sass-loader",
						options: {},
					},
				],
			},
		],
	},

	plugins: [],

	devServer: {

		// Documentation: https://webpack.js.org/configuration/dev-server/
		compress: true,
		contentBase: false,
		historyApiFallback: true,
		// hot: true,
		// hotOnly: true,
		host: "0.0.0.0",
		overlay: {
			warnings: true,
			errors: true,
		},
		port: frontEndConfig.DEV_PORT,
		publicPath: "/",
	},
});
