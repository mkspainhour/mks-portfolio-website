"use strict";

// Node Constructs
const path = require("path");



module.exports = {

	// A single-source-of-truth collection of commonly referenced project files & directories
	source: path.join(__dirname, "../source"),

	frontEnd: path.join(__dirname, "../source/front-end"),
	frontEndEntryPoint: path.join(__dirname, "../source/front-end/index.js"),
	frontEndTemplate: path.join(__dirname, "../source/front-end/index.ejs"),
	favicon: path.join(__dirname, "../source/front-end/favicon.png"),


	buildDirectory: path.join(__dirname, "../build"),

	frontEndConfig: path.join(__dirname, "../source/front-end/config.js"),
	webpackCommonConfig: path.join(__dirname, "./webpack.common.config.js"),
	webpackDevelopmentConfig: path.join(__dirname, "./webpack.development.config.js"),
	webpackProductionConfig: path.join(__dirname, "./webpack.production.config.js"),
	babelConfig: path.join(__dirname, "./babel.config.js"),
	postcssConfig: path.join(__dirname, "./postcss.config.js"),
};
