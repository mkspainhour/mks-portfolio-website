"use strict";

// Webpack Plugins & Loaders
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CssMinimzerPlugin = require("css-minimizer-webpack-plugin");

// Project Constructs
const paths = require("./paths.config");
const { frontEndConfig } = require(paths.frontEndConfig);



module.exports = {

	entry: {
		"front-end": paths.frontEndEntryPoint,
	},

	output: {
		filename: "[name].[contenthash:8].js",
		path: paths.buildDirectory,
		publicPath: ""
	},

	module: {
		rules: [

			// Images
			{
				test: /\.(png|svg|jpeg|jpg|gif)$/i,
				type: "asset/resource",
				generator: {
					filename: "images/[name].[contenthash:8][ext]",
				},
			},

			// Fonts
			{
				test: /\.(woff(2)?|otf|ttf)$/,
				type: "asset/resource",
				generator: {
					filename: "fonts/[name].[contenthash:8][ext]",
				},
			},
		],
	},

	plugins: [

		// Processes index.html or its equivalent template file
		new HtmlWebpackPlugin({
			// Documentation: https://www.npmjs.com/package/html-webpack-plugin
			template: paths.frontEndTemplate,
			templateParameters: {
				TITLE: frontEndConfig.TITLE,
				DESCRIPTION: frontEndConfig.DESCRIPTION,
			},
			filename: "index.html",
			favicon: paths.favicon,
			minify: {
				// Documentation: https://www.npmjs.com/package/html-minifier-terser
				caseSensitive: true,
				collapseBooleanAttributes: true,
				collapseWhitespace: true,
				decodeEntities: true,
				keepClosingSlash: true,
				quoteCharacter: '"',
				removeComments: true,
				removeScriptTypeAttributes: true,
				removeStyleLinkTypeAttributes: true,
			},
		}),
	],

	optimization: {
		minimize: true,
		minimizer: [
			new CssMinimzerPlugin({
				// Documentation: https://www.npmjs.com/package/css-minimizer-webpack-plugin
				minimizerOptions: {
					// Uses cssnano under the hood
					// Documentation: https://github.com/cssnano/cssnano/tree/master/packages
					preset: ["default", {
						convertValues: {
							precision: 3,
						},
					}],
				},
			}),
		],
	},
};
