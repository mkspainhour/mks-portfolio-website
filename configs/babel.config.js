"use strict";



module.exports = function(api) {

	// Documentation: https://babeljs.io/docs/en/config-files#apienv
	const env = api.env();



	const presets = [

		// Babel standard settings preset
		// Documentation: https://babeljs.io/docs/en/babel-preset-env
		[
			"@babel/preset-env",
			{
				targets: "last 2 versions, >=0.2%, not dead",
				bugfixes: true,
				shippedProposals: true,
			}
		],
	];

	const plugins = [];



	return {
		presets,
		plugins
	};
};
