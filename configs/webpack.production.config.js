"use strict";

// Webpack Utilities
const { merge } = require("webpack-merge");

// Webpack Plugins & Loaders
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// Project Constructs
const paths = require("./paths.config");
const commonWebpackConfiguration = require(paths.webpackCommonConfig);



module.exports = merge(commonWebpackConfiguration, {

	mode: "production",

	// Documentation: https://webpack.js.org/configuration/devtool
	devtool: false,

	module: {
		rules: [

			// Javascript (Babel)
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
					options: {
						envName: "production",
						configFile: paths.babelConfig,
					},
				},
			},

			// SASS & CSS
			{
				test: /\.(scss|css)$/,
				use: [

					// Extracts all CSS into per-bundle stylesheets
					{
						// Documentation: https://www.npmjs.com/package/mini-css-extract-plugin
						loader: MiniCssExtractPlugin.loader,
					},

					// Interprets CSS imports
					{
						// Documentation: https://www.npmjs.com/package/css-loader
						loader: "css-loader",
						options: {
							// The number of loaders below this one that should be applied to imported CSS
							importLoaders: 2,
						},
					},

					{
						// Processes CSS through PostCSS
						// Documentation: https://www.npmjs.com/package/postcss-loader
						loader: "postcss-loader",
						options: {
							postcssOptions: {
								config: paths.postcssConfig,
							},
						},
					},

					// Interprets SCSS and CSS files, and compiles SCSS
					{
						// Documentation: https://www.npmjs.com/package/sass-loader
						loader: "sass-loader",
					},
				],
			},
		],
	},

	plugins: [

		// Empties the output folder before adding successfully built files
		new CleanWebpackPlugin({
			// Documentation: https://www.npmjs.com/package/clean-webpack-plugin
			// verbose: true,
		}),

		// Extracts CSS into per-bundle stylesheets
		new MiniCssExtractPlugin({
			// Documentation: https://www.npmjs.com/package/mini-css-extract-plugin
			filename: "[name].[contenthash:8].css",
		}),
	],
});
