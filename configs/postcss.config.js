"use strict";



module.exports = {
   plugins: {

		// A settings preset to be used by PostCSS
		// Documentation: https://www.npmjs.com/package/postcss-preset-env
      "postcss-preset-env": {

			// See: https://cssdb.org/ for an explanation of CSS development stages
			// Documentation: https://preset-env.cssdb.org/features
			stage: 0,

			// Documentation: https://github.com/csstools/postcss-preset-env#browsers
			// Best Practices: https://github.com/browserslist/browserslist#best-practices
			browsers: "last 2 versions, >=0.2%, not dead",
      },
	}
};
